"use strict";
require("dotenv-defaults").config();
const stage = process.env.STAGE || "itest";

const baseDomain = process.env.CDLAB_BASE_DOMAIN || "cdlab.tk";
const schema = process.env.SCHEMA || "http";
const port = process.env.PORT || "8080";

const database = require("knex")({
  client: "pg",
  connection: {
    host: process.env.POSTGRES_HOST,
    database: process.env.POSTGRES_DB,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
  },
  pool: {
    min: 2,
    max: 10,
  },
  migrations: {
    tableName: "knex_migrations",
  },
});

// API Endpoint under Test
const request = require("supertest")(
  `${schema}://api.${stage}.${baseDomain}:${port}`
);

afterAll(() => {
  database.destroy();
});

it("Shoud save a tutorial to the database", async () => {
  const randomString = Math.random();
  const data = {
    title: `CD Tutorial ${randomString}`,
    description: "First CD Tutorial",
  };

  const expectedTitle = `Cd Tutorial ${randomString}`;

  const res = await request.post("/api/tutorials").send(data);

  // check http
  expect(res.status).toBe(200);
  expect(res.body.title).toBe(expectedTitle);

  // display db content
  const rows = await database.select().from("tutorials");
  console.log("rows", rows);

  // check db content
  const result = await database
    .count("title")
    .from("tutorials")
    .where("title", expectedTitle);

  expect(result[0].count).toBe("1");
});
