"use strict";

const { capitalizeTitle } = require("../tutorial.controller");

describe("testing controller", () => {
  test("1: testing function 'capitalizeTitle'", async () => {
    const title = "this is a boring title";

    expect(capitalizeTitle(title)).toBe("This Is A Boring Title");
  });
});
