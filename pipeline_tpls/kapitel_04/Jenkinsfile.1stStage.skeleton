pipeline {
    agent any
    options {
        gitLabConnection('cdlab')
        skipStagesAfterUnstable()
        skipDefaultCheckout()
        parallelsAlwaysFailFast()
        // preserveStashes() // enable for debugging
    }
    stages {
        stage("1-CS Checkout") {
            agent none
            steps {
                gitlabCommitStatus("Checkout") {
                    sh 'printenv | sort | tee vars'
                    sh '''
                    set +x
                    echo "---------------------------------------------------------"
                    echo "==         C H E C K O U T   S O U R C E S             =="
                    echo "---------------------------------------------------------"
                    set -x
                    '''
                    sh 'ls -al'
                    stash name: "project"
                }
            }
        }
        stage("1-CS Build") {
            agent none
            steps {
                gitlabCommitStatus("Build") {
                    sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                    unstash 'project'
                    sh '''
                    set +x
                    echo "---------------------------------------------------------"
                    echo "==                     B U I L D                       =="
                    echo "---------------------------------------------------------"
                    set -x
                    '''
                    sh 'ls -al'
                    stash name: "project"
                }
            }
        }
        stage("1-CS Unittest & StaticAnalysis"){
            failFast true
            parallel {
                stage("Lint") {
                    agent none
                    steps {
                        gitlabCommitStatus("Lint") {
                            sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                            unstash 'project'

                            sh '''
                                set +x
                                echo "---------------------------------------------------------"
                                echo "==      S T A T I C   C O D E   A N A L Y S I S        =="
                                echo "---------------------------------------------------------"
                                set -x
                            '''
                        }
                    }
                }
                stage("Unittest") {
                    agent none
                    steps {
                        gitlabCommitStatus("Unittest") {
                            sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                            unstash 'project'
                            sh '''
                                set +x
                                echo "---------------------------------------------------------"
                                echo "==               U N I T   T E S T S                   =="
                                echo "---------------------------------------------------------"
                                set -x
                            '''
                        }
                    }
                }
            }
        }
        stage("1-CS Package & Upload Artifact"){
            agent none
            steps {
                gitlabCommitStatus("Package") {
                    sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                    unstash 'project'

                    sh '''
                        set +x
                        echo "---------------------------------------------------------"
                        echo "==  P A C K A G E   &   U P L O A D   A R T I F A C T  =="
                        echo "---------------------------------------------------------"
                        set -x
                    '''
                }
            }
        }
    }
}
