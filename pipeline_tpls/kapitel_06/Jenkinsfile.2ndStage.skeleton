pipeline {
    agent any
    options {
        gitLabConnection('cdlab')
        skipStagesAfterUnstable()
        skipDefaultCheckout()
        parallelsAlwaysFailFast()
        // preserveStashes() // enable for debugging
    }
    environment {
        baseDomain = 'cdlab.tk'
        dockerRegistryHost = "registry.${baseDomain}"
        gitlabHost = "gitlab.${baseDomain}"
        gitlabGroup = 'tutorials'
        gitlabProject = 'tutorials-api'
        serviceName = "${gitlabProject}"
        servicePort = 8080
    }
    stages {
        stage("1-CS Checkout") {
            agent none
            steps {
                gitlabCommitStatus("Checkout") {
                    sh 'printenv | sort | tee vars'
                    sh '[ -f buildvars ] && rm -f buildvars'
                    sh '''
                    set +x
                    echo "---------------------------------------------------------"
                    echo "==         C H E C K O U T   S O U R C E S             =="
                    echo "---------------------------------------------------------"
                    set -x
                    '''
                    checkout(
                        changelog: false,
                        poll: false,
                        scm: [$class: 'GitSCM',
                            branches: [[name: '${gitlabAfter}']],
                            extensions: [],
                            userRemoteConfigs: [[credentialsId: 'GITLAB_USER_JENKINS', url: 'https://gitlab.cdlab.tk/tutorials/tutorials-api.git']]
                        ]
                    )
                    sh 'ls -al'
                    withCredentials([usernamePassword(credentialsId: 'GITLAB_USER_JENKINS', passwordVariable: 'regPassword', usernameVariable: 'regUser')]) {
                        script {
                            gitBranchNormalized = env.gitlabBranch.replace(/[^a-zA-Z0-9_.]/, '-').take(128)
                            gitCommitShaShort = env.gitlabAfter.take(8)
                            dockerImageTag = "${gitBranchNormalized}-${gitCommitShaShort}"
                            dockerImageName = "${dockerRegistryHost}/${gitlabGroup}/${gitlabProject}"
                            env.dockerImageFQN = "${dockerImageName}:${dockerImageTag}"

                            env.dockerRegistryUser = regUser
                            env.dockerRegistryPassword = regPassword
                        }
                    }
                    sh '''
                        set +x
                        buildArtifactAlreadyExists=$(scripts/docker-image-already-exists.sh "${dockerImageFQN}" "${dockerRegistryHost}" "${dockerRegistryUser}" "${dockerRegistryPassword}" "${gitlabHost}" 2>output)
                        cat output
                        set -x
                        echo "env.buildArtifactAlreadyExists='${buildArtifactAlreadyExists}'" >> buildvars
                    '''

                    load 'buildvars'
                    stash name: "project"
                }
            }
        }
        stage("1-CS Build") {
            agent { docker 'node:14.16.1' }
            when {
                environment name: 'buildArtifactAlreadyExists', value: 'false';
            }
            steps {
                gitlabCommitStatus("Build") {
                    sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                    unstash 'project'
                    sh '''
                    set +x
                    echo "---------------------------------------------------------"
                    echo "==                     B U I L D                       =="
                    echo "---------------------------------------------------------"
                    set -x
                    '''
                    sh 'ls -al'
                    sh 'npm ci'
                    stash name: "project"
                }
            }
        }
        stage("1-CS Unittest & StaticAnalysis"){
            failFast true
            parallel {
                stage("Lint") {
                    agent { docker 'node:14.16.1' }
                    when {
                        environment name: 'buildArtifactAlreadyExists', value: 'false';
                    }
                    steps {
                        gitlabCommitStatus("Lint") {
                            sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                            unstash 'project'

                            sh '''
                                set +x
                                echo "---------------------------------------------------------"
                                echo "==      S T A T I C   C O D E   A N A L Y S I S        =="
                                echo "---------------------------------------------------------"
                                set -x
                            '''
                            sh 'npm run lint'
                            sh 'npm run prettier:ci'
                        }
                    }
                }
                stage("Unittest") {
                    agent { docker 'node:14.16.1' }
                    when {
                        environment name: 'buildArtifactAlreadyExists', value: 'false';
                    }
                    steps {
                        gitlabCommitStatus("Unittest") {
                            sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                            unstash 'project'
                            sh '''
                                set +x
                                echo "---------------------------------------------------------"
                                echo "==               U N I T   T E S T S                   =="
                                echo "---------------------------------------------------------"
                                set -x
                            '''
                            sh 'npm run test:ci'
                        }
                    }
                    post {
                        always {
                            junit allowEmptyResults: true, testResults: 'reports/junit/*.xml'
                        }
                    }
                }
            }
        }
        stage("1-CS Package & Upload Artifact"){
            agent none
            when {
                environment name: 'buildArtifactAlreadyExists', value: 'false';
            }
            steps {
                gitlabCommitStatus("Package") {
                    sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                    unstash 'project'

                    sh '''
                        set +x
                        echo "---------------------------------------------------------"
                        echo "==  P A C K A G E   &   U P L O A D   A R T I F A C T  =="
                        echo "---------------------------------------------------------"
                        set -x
                    '''
                    withCredentials([usernamePassword(credentialsId: 'GITLAB_USER_JENKINS', passwordVariable: 'dockerRegistryPassword', usernameVariable: 'dockerRegistryUser')]) {
                        sh '''
                            docker build -f Dockerfile.deployment -t $dockerImageFQN .
                            set +x
                            echo "${dockerRegistryPassword}" | docker login ${dockerRegistryHost} -u${dockerRegistryUser} --password-stdin
                            set -x
                            docker push $dockerImageFQN
                        '''
                    }
                }
            }
        }
        stage("2-Deploy on IntTest Env"){
            agent none
            steps {
                gitlabCommitStatus("Deploy IntTest") {
                    sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                    unstash 'project'
                    sh '''
                        set +x
                        echo "---------------------------------------------------------"
                        echo "==     D E P L O Y   O N   I N T T E S T   E N V       =="
                        echo "---------------------------------------------------------"
                        set -x
                    '''
                }
            }
        }
    }
}
