pipeline {
    agent any
    options {
        gitLabConnection('cdlab')
        skipStagesAfterUnstable()
        skipDefaultCheckout()
        parallelsAlwaysFailFast()
        // preserveStashes() // enable for debugging
    }
    environment {
        baseDomain = 'cdlab.tk'
        dockerRegistryHost = "registry.${baseDomain}"
        gitlabHost = "gitlab.${baseDomain}"
        gitlabGroup = 'tutorials'
        gitlabProject = 'tutorials-api'
        serviceName = "${gitlabProject}"
        servicePort = 8080
        sshOpts= '-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'
    }
    stages {
        stage("1-CS Checkout") {
            agent none
            steps {
                gitlabCommitStatus("Checkout") {
                    sh 'printenv | sort | tee vars'
                    sh '[ -f buildvars ] && rm -f buildvars'
                    sh '''
                    set +x
                    echo "---------------------------------------------------------"
                    echo "==         C H E C K O U T   S O U R C E S             =="
                    echo "---------------------------------------------------------"
                    set -x
                    '''
                    checkout(
                        changelog: false,
                        poll: false,
                        scm: [$class: 'GitSCM',
                            branches: [[name: '${gitlabAfter}']],
                            extensions: [],
                            userRemoteConfigs: [[credentialsId: 'GITLAB_USER_JENKINS', url: 'https://gitlab.cdlab.tk/tutorials/tutorials-api.git']]
                        ]
                    )
                    withCredentials([usernamePassword(credentialsId: 'GITLAB_USER_JENKINS', passwordVariable: 'dockerRegistryPassword', usernameVariable: 'dockerRegistryUser')]) {
                        
                        // handle regular build (pushes on any branch, no tagging) and tag pushes
                        sh '''
                            dockerImageFQN=""
                            dockerImageFQNRelease=""
                            dockerImageTagBranchHash=""
                            dockerImageTagSemver=""
                            gitBranchNormalized="unknown"
                            buildArtifactAlreadyExists="false"

                            gitCommitShaShort=$(echo $gitlabAfter | cut -c1-8) # default - changed when tagged!
                            dockerImageName="${dockerRegistryHost}/${gitlabGroup}/${gitlabProject}"

                            if echo "$gitlabBranch" | grep '^refs/tags/.*$';then
                                dockerImageTagSemver=$(echo "$gitlabBranch" | sed 's;^refs/tags/;;g')
                                gitCommitShaShort=$(git rev-list -n 1 $dockerImageTagSemver | cut -c1-8)
                                releaseArtifactAlreadyExists=$(scripts/docker-image-already-exists.sh "${dockerImageName}:${dockerImageTagSemver}" "${dockerRegistryHost}" "${dockerRegistryUser}" "${dockerRegistryPassword}" "${gitlabHost}" 2>output)
                                cat output

                                if [ "$releaseArtifactAlreadyExists" != "false" ];then
                                    echo "ERROR: Release Artifact Docker Image already exists!"
                                    echo "Please use another Release Tag ($dockerImageTagSemver is already in use!)!"
                                    exit 1
                                fi

                                # getting corresponding docker non-release artifact if exists
                                correspondingDockerImageTag=$(scripts/docker-image-already-exists.sh "${dockerImageName}:*-${gitCommitShaShort}" "${dockerRegistryHost}" "${dockerRegistryUser}" "${dockerRegistryPassword}" "${gitlabHost}" 2>output)
                                cat output
                                if [ "$correspondingDockerImageTag" != "false" ];then
                                    gitBranchNormalized=$(echo $correspondingDockerImageTag | sed 's;-[^-]*$;;g')
                                    echo "Found corresponding existing Docker Image with Tag ${dockerImageName}:*-${gitCommitShaShort}."
                                    echo "Setting gitBranchNormalized=$gitBranchNormalized"
                                fi
                            else
                                gitBranchNormalized=$(echo $gitlabBranch | sed 's;[^a-zA-Z0-9_.];-;g' | cut -c1-119)
                            fi

                            dockerImageTagBranchHash="${gitBranchNormalized}-${gitCommitShaShort}"
                            dockerImageFQN="${dockerImageName}:${dockerImageTagBranchHash}"
                            if [ -n "$dockerImageTagSemver" ];then
                                dockerImageFQNRelease="${dockerImageName}:${dockerImageTagSemver}"
                            fi

                            # Replace actual DockerImageFQN in docker-compose.yml
                            dockerImageFQNThis=$dockerImageFQN
                            if [ -n "$dockerImageFQNRelease" ];then
                                dockerImageFQNThis=$dockerImageFQNRelease
                            fi
                            sed -i "s;__DOCKER_IMAGE_FQN__;${dockerImageFQNThis};g" docker-compose.yml

                            # Set buildArtifactAlreadyExists to build only once
                            buildArtifactAlreadyExists=$(scripts/docker-image-already-exists.sh "${dockerImageFQN}" "${dockerRegistryHost}" "${dockerRegistryUser}" "${dockerRegistryPassword}" "${gitlabHost}" 2>output)
                            cat output
                            
                            # publish vars as environment vars
                            echo "env.dockerImageFQNThis='${dockerImageFQNThis}'" >> buildvars
                            echo "env.dockerImageFQN='${dockerImageFQN}'" >> buildvars
                            echo "env.dockerImageFQNRelease='${dockerImageFQNRelease}'" >> buildvars
                            echo "env.buildArtifactAlreadyExists='${buildArtifactAlreadyExists}'" >> buildvars
                            cat buildvars
                        '''
                    }

                    load 'buildvars'
                    stash name: "project"
                }
            }
        }
        stage("1-CS Build") {
            agent { docker 'node:14.16.1' }
            when {
                environment name: 'buildArtifactAlreadyExists', value: 'false';
            }
            steps {
                gitlabCommitStatus("Build") {
                    sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                    unstash 'project'
                    sh '''
                    set +x
                    echo "---------------------------------------------------------"
                    echo "==                     B U I L D                       =="
                    echo "---------------------------------------------------------"
                    set -x
                    '''
                    sh 'ls -al'
                    sh 'npm ci'
                    stash name: "project"
                }
            }
        }
        stage("1-CS Unittest & StaticAnalysis"){
            failFast true
            parallel {
                stage("Lint") {
                    agent { docker 'node:14.16.1' }
                    when {
                        environment name: 'buildArtifactAlreadyExists', value: 'false';
                    }       
                    steps {
                        gitlabCommitStatus("Lint") {
                            sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                            unstash 'project'

                            sh '''
                                set +x
                                echo "---------------------------------------------------------"
                                echo "==      S T A T I C   C O D E   A N A L Y S I S        =="
                                echo "---------------------------------------------------------"
                                set -x
                            '''
                            sh 'npm run lint'
                            sh 'npm run prettier:ci'
                        }
                    }
                }
                stage("Unittest") {
                    agent { docker 'node:14.16.1' }
                    when {
                        environment name: 'buildArtifactAlreadyExists', value: 'false';
                    }       
                    steps {
                        gitlabCommitStatus("Unittest") {
                            sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                            unstash 'project'
                            sh '''
                                set +x
                                echo "---------------------------------------------------------"
                                echo "==               U N I T   T E S T S                   =="
                                echo "---------------------------------------------------------"
                                set -x
                            '''
                            sh 'npm run test:ci'
                        }
                    }
                    post {
                        always {
                            junit allowEmptyResults: true, testResults: 'reports/junit/*.xml'
                        }
                    }
                }
            }
        }
        stage("1-CS Package & Upload Artifact"){
            agent none
            when {
                environment name: 'buildArtifactAlreadyExists', value: 'false';
            }
            steps {
                gitlabCommitStatus("Package") {
                    sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                    unstash 'project'

                    sh '''
                        set +x
                        echo "---------------------------------------------------------"
                        echo "==  P A C K A G E   &   U P L O A D   A R T I F A C T  =="
                        echo "---------------------------------------------------------"
                        set -x
                    '''
                    withCredentials([usernamePassword(credentialsId: 'GITLAB_USER_JENKINS', passwordVariable: 'dockerRegistryPassword', usernameVariable: 'dockerRegistryUser')]) {
                        sh '''
                            docker build -f Dockerfile.deployment -t $dockerImageFQN .
                            set +x
                            echo "${dockerRegistryPassword}" | docker login ${dockerRegistryHost} -u${dockerRegistryUser} --password-stdin
                            set -x
                            docker push $dockerImageFQN
                        '''
                    }
                }
            }
        }
        stage("1-CS Tag Release Artifact"){
            agent none
            when {
                expression {
                    return env.dockerImageFQNRelease != '';
                }
            }
            steps {
                gitlabCommitStatus("Tag") {
                    sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                    unstash 'project'

                    sh '''
                        set +x
                        echo "---------------------------------------------------------"
                        echo "==  T A G   R E L E A S E   A R T I F A C T  =="
                        echo "---------------------------------------------------------"
                        set -x
                    '''
                    withCredentials([usernamePassword(credentialsId: 'GITLAB_USER_JENKINS', passwordVariable: 'dockerRegistryPassword', usernameVariable: 'dockerRegistryUser')]) {
                        sh '''
                            set +x
                            echo "${dockerRegistryPassword}" | docker login ${dockerRegistryHost} -u${dockerRegistryUser} --password-stdin
                            set -x
                            docker pull $dockerImageFQN
                            docker tag $dockerImageFQN $dockerImageFQNRelease
                            docker push $dockerImageFQNRelease
                        '''
                    }
                }
            }
        }
        stage("2-Deploy on IntTest Env"){
            agent none
            steps {
                gitlabCommitStatus("Deploy IntTest") {
                    sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                    unstash 'project'
                    sh '''
                        set +x
                        echo "---------------------------------------------------------"
                        echo "==     D E P L O Y   O N   I N T T E S T   E N V       =="
                        echo "---------------------------------------------------------"
                        set -x
                    '''
                    withCredentials([sshUserPrivateKey(credentialsId: 'SERVICEUSER_SSH', keyFileVariable: 'identityFile', passphraseVariable: '', usernameVariable: '')]) {
                        sh 'ssh ${sshOpts} -i ${identityFile} serviceuser@dockerhost.itest.${baseDomain} deploy-service.sh $serviceName $dockerImageFQNThis'
                    }
                    sh 'scripts/wait-for.sh api.itest.${baseDomain}:$servicePort -t 30 -- echo "Service $serviceName on env itest up and running!"'
                }
            }
        }
        stage("2-Integration Test"){
            agent { docker 'node:14.16.1' }
            steps {
                gitlabCommitStatus("Integration Test") {
                    sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                    unstash 'project'
                    sh '''
                        set +x
                        echo "---------------------------------------------------------"
                        echo "==      I N T E G R A T I O N   T E S T S              =="
                        echo "---------------------------------------------------------"
                        set -x
                    '''
                    withCredentials([string(credentialsId: 'TUTORIALS_API_POSTGRES_PASSWORD', variable: 'postgresPassword')]) {
                        sh '''
                            postgresHost="db.itest.${baseDomain}"
                            cp .env.itest .env
                            sed -i "s;__POSTGRES_HOST__;$postgresHost;g" .env
                            sed -i "s;__POSTGRES_PASSWORD__;$postgresPassword;g" .env
                            npm ci
                            npm run itest:ci
                        '''
                    }
                }
            }
            post {
                always {
                    junit allowEmptyResults: true, testResults: 'reports/itest/*.xml'
                }
            }
        }
        stage("3-Deploy on Staging Env"){
            agent none
            when {
                expression {
                    return env.dockerImageFQNRelease != '';
                }
            }
            steps {
                gitlabCommitStatus("Deploy Staging") {
                    sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                    unstash 'project'
                    sh '''
                        set +x
                        echo "---------------------------------------------------------"
                        echo "==     D E P L O Y   O N   S T A G I N G   E N V       =="
                        echo "---------------------------------------------------------"
                        set -x
                    '''
                    withCredentials([sshUserPrivateKey(credentialsId: 'SERVICEUSER_SSH', keyFileVariable: 'identityFile', passphraseVariable: '', usernameVariable: '')]) {
                        sh 'ssh ${sshOpts} -i ${identityFile} serviceuser@dockerhost.staging.${baseDomain} deploy-service.sh $serviceName $dockerImageFQNThis'
                    }
                    sh 'scripts/wait-for.sh api.staging.${baseDomain}:$servicePort -t 30 -- echo "Service $serviceName on env staging up and running!"'
                }
            }
        }
        stage("4-Deploy on Prod Env Confirmation"){
            agent none
            when {
                expression {
                    return env.dockerImageFQNRelease != '';
                }
            }
            steps {
                gitlabCommitStatus("Wait for confirmation") {
                    timeout(time: 12, unit: "HOURS") {
                        script {
                            input message: "Deploy on Prod?", ok: 'Do it!'
                        }
                    }
                }
            }
        }
        stage("4-Deploy on Prod Env"){
            agent none
            when {
                expression {
                    return env.dockerImageFQNRelease != '';
                }
            }
            steps {
                gitlabCommitStatus("Deploy Prod") {
                    sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                    unstash 'project'
                    sh '''
                        set +x
                        echo "---------------------------------------------------------"
                        echo "==     D E P L O Y   O N   P R O D   E N V             =="
                        echo "---------------------------------------------------------"
                        set -x
                    '''
                    withCredentials([sshUserPrivateKey(credentialsId: 'SERVICEUSER_SSH', keyFileVariable: 'identityFile', passphraseVariable: '', usernameVariable: '')]) {
                        sh 'ssh ${sshOpts} -i ${identityFile} serviceuser@dockerhost.prod.${baseDomain} deploy-service.sh $serviceName $dockerImageFQNThis'
                    }
                    sh 'scripts/wait-for.sh api.prod.${baseDomain}:$servicePort -t 30 -- echo "Service $serviceName on env prod up and running!"'
                }
            }
        }    
    }
}
