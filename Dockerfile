FROM node:14.16.1

WORKDIR /opt/node/app

COPY package* ./
COPY app ./app/
COPY node_modules ./node_modules/
COPY index.js ./

EXPOSE 8080

USER node

CMD [ "npm", "start" ]
