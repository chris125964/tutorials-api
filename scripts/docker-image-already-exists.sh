#!/usr/bin/env bash
SCRIPTS="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

DOCKER_IMAGE_FQN="$1"
REGISTRY_HOST="$2"
REGISTRY_USER="$3"
REGISTRY_PASSWORD="$4"
GITLAB_HOST="$5"

SHOW_USAGE=0
if [ -z "$DOCKER_IMAGE_FQN" ];then
  echo "ERROR: ARG1 is empty!"
  SHOW_USAGE=1
fi

if [ -z "$REGISTRY_HOST" ];then
  echo "ERROR: ARG2 is empty!"
  SHOW_USAGE=1
fi

if [ -z "$REGISTRY_USER" ];then
  echo "ERROR: ARG3 is empty!"
  SHOW_USAGE=1
fi

if [ -z "$REGISTRY_PASSWORD" ];then
  echo "ERROR: ARG4 is empty!"
  SHOW_USAGE=1
fi

if [ -z "$GITLAB_HOST" ];then
  echo "ERROR: ARG5 is empty!"
  SHOW_USAGE=1
fi

if [ $SHOW_USAGE -eq 1 ];then
  echo "Usage: $0 \"<docker image name>\" \"<registry host>\" \"<registry username>\" \"<registry password>\" \"<gitlab host>\""
  exit 1
fi

# ------ main
DOCKER_IMAGE_NAME=$(echo $DOCKER_IMAGE_FQN | awk -F ':' '{print $1;}' | sed "s;^${REGISTRY_HOST}/;;g")
DOCKER_IMAGE_TAG=$(echo $DOCKER_IMAGE_FQN | awk -F ':' '{print $2;}')

DOCKER_IMAGE_TAG_SEARCH_REGEX="^${DOCKER_IMAGE_TAG}$"
if grep '*' <<< $DOCKER_IMAGE_TAG >/dev/null ;then
  DOCKER_IMAGE_TAG_SEARCH_REGEX="^$(echo $DOCKER_IMAGE_TAG | sed 's;*;.*;g')$"
fi

echo "DOCKER_IMAGE_NAME=$DOCKER_IMAGE_NAME" >&2
echo "DOCKER_IMAGE_TAG=$DOCKER_IMAGE_TAG" >&2
echo "DOCKER_IMAGE_TAG_SEARCH_REGEX=$DOCKER_IMAGE_TAG_SEARCH_REGEX" >&2

TOKEN=$(curl -sS --user "${REGISTRY_USER}:${REGISTRY_PASSWORD}" "https://${GITLAB_HOST}/jwt/auth?client_id=docker&offline_token=true&service=container_registry&scope=repository:${DOCKER_IMAGE_NAME}:push,pull"| jq -r .token)
IFS=
FOUND=$(curl -sS -H 'Accept: application/vnd.docker.distribution.manifest.v2+json' -H "Authorization: Bearer $TOKEN" https://${REGISTRY_HOST}/v2/${DOCKER_IMAGE_NAME}/tags/list?page_size=10000 | jq -r .tags[] | grep -E "$DOCKER_IMAGE_TAG_SEARCH_REGEX")
FOUND_COUNT=$(echo -n $FOUND | grep -c '^')

echo "Image Tag(s) (count: $FOUND_COUNT) found:" >&2
echo "$FOUND" >&2

if [ $FOUND_COUNT -gt 1 ];then
  echo "ERROR: Ambiguous number of docker images with search regex $DOCKER_IMAGE_TAG_SEARCH_REGEX found! Please narrow down search!" >&2
  exit 1
fi

if [ $FOUND_COUNT -eq 1 ];then
  echo -n "$FOUND"
else
  echo -n "false"
fi

exit 0
