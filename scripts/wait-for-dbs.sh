#!/usr/bin/env bash
set -e

# Comma separated String of databses to wait for
# Example:
# viewdb:3306,messagedb:5432,postgres:5432

DATABASES_STRING=$1
IFS=',' read -r -a DATABASES_ARRAY <<< "$DATABASES_STRING"

for DB in "${DATABASES_ARRAY[@]}";do
  echo "Waiting for $DB..."
  ./scripts/wait-for.sh $DB -t 30
done

echo "All Databases are reachable."
echo "Waiting another 5s to let them settle..."
sleep 5
echo "Now starting up node Server..."

npm start
