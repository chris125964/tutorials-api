#!/bin/bash

TARGET_STAGE=$1
if [ -z "$TARGET_STAGE" ];then
  echo "ERROR: Missing ARG1 -> TARGET_STAGE (pos. values: dev, ci, staging, prod) - usage: truncate-all-dbs.sh <TARGET_STAGE> <SSH_USER>"
  exit 1
fi

SSH_USER=$2
if [ -z "$SSH_USER" ];then
  SSH_USER=$USER
fi
if [ -z "$SSH_USER" ];then
  echo "ERROR: Missing ARG2 -> SSH_USER - usage: truncate-all-dbs.sh <TARGET_STAGE> <SSH_USER>"
  exit 1
fi

SSH_OPTS="-o StrictHostKeyChecking=no -o LogLevel=ERROR"
SSH="ssh -A $SSH_OPTS $SSH_USER@__TARGET_HOST__ -C"

DEFAULT_BASE_DOMAIN="cdlab.tk"
LOCAL_BASE_DOMAIN="cdlab.local"

case "$TARGET_STAGE" in
  prod)
    API_HOST="api.${TARGET_STAGE}.${DEFAULT_BASE_DOMAIN}"
    ;;
  staging)
    API_HOST="api.${TARGET_STAGE}.${DEFAULT_BASE_DOMAIN}"
    ;;
  local)
    API_HOST="api.${TARGET_STAGE}.${LOCAL_BASE_DOMAIN}"
    ;;
  *)
    API_HOST="api.${TARGET_STAGE}.${DEFAULT_BASE_DOMAIN}"
    ;;
esac

echo "Testing SSH Login with User $SSH_USER..."
SSH_API=$(echo $SSH | sed "s/__TARGET_HOST__/$API_HOST/g")

# $SSH_API "echo '${API_HOST}: Login with User ${SSH_USER} successful.'"

echo "Testing passwordless sudo with User $SSH_USER..."
$SSH_API "sudo echo '${API_HOST}: Login plus sudo with User ${SSH_USER} successful.'"
echo

# ----- db (postgres)
echo "Truncating DB (postgres)..."
CMD_1="sudo echo not implemented yet"
$SSH_API "$CMD_1"
echo "Done."
echo
