pipeline {
    agent any
    post {
        failure {
            updateGitlabCommitStatus name: 'Overall', state: 'failed'
        }
        success {
            updateGitlabCommitStatus name: 'Overall', state: 'success'
        }
    }
    options {
        gitLabConnection('cdlab')
        skipStagesAfterUnstable()
        skipDefaultCheckout()
        parallelsAlwaysFailFast()
        // preserveStashes() // enable for debugging
    }
    environment {
    }
    stages {
        stage("1-CS Checkout & Build") {
            agent none
            steps {
                gitlabCommitStatus("Build") {
                    sh 'printenv | sort | tee vars'
                    sh '''
                    set +x
                    echo "---------------------------------------------------------"
                    echo "==         C H E C K O U T   S O U R C E S             =="
                    echo "---------------------------------------------------------"
                    set -x
                    '''
                    stash name: "project"
                }
            }
        }
        stage("1-CS Unittest & StaticAnalysis"){
            failFast true
            parallel {
                stage("Lint") {
                    agent none
                    steps {
                        gitlabCommitStatus("Lint") {
                            sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                            unstash 'project'

                            sh '''
                                set +x
                                echo "---------------------------------------------------------"
                                echo "==      S T A T I C   C O D E   A N A L Y S I S        =="
                                echo "---------------------------------------------------------"
                                set -x
                            '''
                        }
                    }
                }
                stage("Unittest") {
                    agent none
                    steps {
                        gitlabCommitStatus("Unittest") {
                            sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                            unstash 'project'
                            sh '''
                                set +x
                                echo "---------------------------------------------------------"
                                echo "==               U N I T   T E S T S                   =="
                                echo "---------------------------------------------------------"
                                set -x
                            '''
                        }
                    }
                }
            }
        }
        stage("1-CS Package & Upload Artifact"){
            agent none
            steps {
                gitlabCommitStatus("Package") {
                    sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                    unstash 'project'

                    sh '''
                        set +x
                        echo "---------------------------------------------------------"
                        echo "==  P A C K A G E   &   U P L O A D   A R T I F A C T  =="
                        echo "---------------------------------------------------------"
                        set -x
                    '''
                }
            }
        }
        stage("2-Deploy on IntTest Env"){
            agent none
            steps {
                gitlabCommitStatus("Deploy IntTest") {
                    sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                    unstash 'project'
                    sh '''
                        set +x
                        echo "---------------------------------------------------------"
                        echo "==     D E P L O Y   O N   I N T T E S T   E N V       =="
                        echo "---------------------------------------------------------"
                        set -x
                    '''
                }
            }
        }
        stage("2-Integration Test"){
            agent none
            steps {
                gitlabCommitStatus("Integration Test") {
                    sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                    unstash 'project'
                    sh '''
                        set +x
                        echo "---------------------------------------------------------"
                        echo "==      I N T E G R A T I O N   T E S T S              =="
                        echo "---------------------------------------------------------"
                        set -x
                    '''
                }
            }
        }
        stage("3-Deploy on Public Env"){
            agent none
            steps {
                gitlabCommitStatus("Deploy Public") {
                    sh 'ls -a -1 | grep -v ^\\.\\.$ | grep -v ^\\.$ | xargs rm -rf' // cleanup workspace
                    unstash 'project'
                    sh '''
                        set +x
                        echo "---------------------------------------------------------"
                        echo "==       D E P L O Y   O N   P U B L I C   E N V       =="
                        echo "---------------------------------------------------------"
                        set -x
                    '''
                }
            }
        }
    }
}
